def generator_example():
    yield 1
    yield "Init generator"
    yield "..."
    raise ZeroDivisionError("example exception")
    for i in range(3):
        yield i

def iter_example():
    ret = []
    ret.append("This is the beggining")
    ret.append("computing...")
    ret.append("All done")
    return ret


def from_one_to_inf():
    i = 1
    while True:
        yield i
        i += 1


if __name__ == "__main__":    
    ## read item by item using next function
    # generator = generator_example()
    # print(type(generator))
    # print(next(generator))
    # next(generator)
    # print(next(generator))
    # print(next(generator))
    # print(next(generator))
    
    ## usual way to go through generator elements
    # for g in generator:
    #     print(g)

    ## infinite while loop, but possible to iterate because it's a generator
    for i in from_one_to_inf():
        print(i)
        if i > 50:
            break
        
        
    ## example with catching different exception from generator
    generator = generator_example()
    while True:
        try:
            item = next(generator)
            print(item)
        except StopIteration as e:
            print("all read")
            break
        except Exception as e:
            print("Other exception")
            print(e)

    ## infinite loop, never finishes
    # list(from_one_to_inf())
    # print("all done")

