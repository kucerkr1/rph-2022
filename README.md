# RPH
Stránky předmětu:
[https://cw.fel.cvut.cz/b211/courses/b4b33rph/start](https://cw.fel.cvut.cz/b211/courses/b4b33rph/start)

### CV01 z předchozího roku
- [kódy](cv01-old)


### CV02 z předchozího roku
- [kódy](cv02-old)

### CV03 z předchozího roku
Konzultační cvičení, opakování větvení, cyklů, tříd. Implementace hry "šibenice"

- [kódy](cv03-old)

### CV04 z předchozího roku
- [kódy](cv04-old)

### CV05
Procházení 2D matice ve směru specifikovaném jako `(add_r, add_c)`

- [kódy](cv05)

### CV06
Game of Life

- [kódy](cv06)

### CV07
Práce se soubory, ukázka problémů při špatném kódování, počítání znaků ve stringu

- [kódy](cv07)


### CV08
Generátory vs. iterátory

- [kódy](cv08)

### Týmová úloha SPAM

Při práci v týmu můžete použít `git` nebo jiný verzovací systém - pro případ že si řešení rozbijete / zhoršíte je více než vhodné mít zálohu.

- [**Kurz od FIT**](https://courses.fit.cvut.cz/BI-GIT/) - je možné přihlásit pomocí ČVUT účtu
- [**LaTeX Šablona na Overleafu**](https://www.overleaf.com/read/ygnfhtdmzkzb) - pro ukázku, použijte libovolnou
- [Co dělají začátečníci v Git](https://www.youtube.com/watch?v=AysWlOWXIts) a čeho je dobré se vyvarovat.

Také je možné použít doplňky pro sdílení kódu - např:
- [**Code With Me**](https://www.jetbrains.com/code-with-me/) v PyCharm, naleznete pod menu Tools
- [**LiveShare**](https://marketplace.visualstudio.com/items?itemName=MS-vsliveshare.vsliveshare) do [Visual Studio Code](https://code.visualstudio.com/)
