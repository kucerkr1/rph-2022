from random import randint

def print_data(data):
    """Print data of a matrix without extra symbols."""
    for row in data:
        for elem in row:
            print(elem, end="  ")
        print()

def print_data_aligned(data):
    """Print data of a matrix without extra symbols,
        aligned to the right by the longest element in string. """
    max_len = max(len(str(elem)) for row in data for elem in row)
    for row in data:
        for elem in row:
            l = max_len - len(str(elem))
            print(" " * l, elem, sep="", end="")
        print()

def generate_data(n_rows, n_cols):
    """Create a matrix of size (n_rows, n_cols)
        filled with values 0 and 1 at random."""
    data = []
    for r in range(n_rows):
        row = []
        for c in range(n_cols):
            value = randint(0, 1)
            row.append(value)
        data.append(row)
    ## Alternative with list comprehension
    data = [[randint(0, 1) for c in range(n_cols)]
                for r in range(n_rows)]
    return data

def traverse_row(row, value, start_idx, end_idx, step):
    """Traverse a list with range by input params
    and compare with input value
        The docstring is written in with PEP 287 guidelines

        :param row: list
        :param value: value for comparison
        :param start_idx: initial index of array
        :param end_idx: final index of range (idx of array + 1)
        :param step: range increment
        :return: number of values in continuous neighborhood.
    """
    ## Test the docstring using import main; help(traverse_row)
    count = 0
    for i in range(start_idx, end_idx, step):
        if value == row[i]:
            count += 1
        else:
            break
    return count

def traverse_dir(data, row, col, dir):
    """Traverse 2D matrix from index(row, col) by the specified dir."""
    count = 0
    value = data[row][col]
    n_rows = len(data)
    n_cols = len(data[row])
    r = row + dir[0]
    c = col + dir[1]
    while r >= 0 and r < n_rows and c >= 0 and c < n_cols:
        if value == data[r][c]:
            count += 1
        else:
            break
        r = r + dir[0]
        c = c + dir[1]
    return count

DIRS = [(-1, 0), (1, 0), (0, 1), (0, -1), (-1, 1), (-1, -1), (1, 1), (1, -1)]

def line_column_size(row, col, data):
    """Count the number of same values in proximity of index (row, col)
        when traversing row-wise or column-wise."""
    if row >= len(data) or col >= len(data[row]):
        raise IndexError("Index is outside of data range")
    dirs = [(-1, 0), (1, 0), (0, 1), (0, -1)]
    count = 1  ## always min. 1 for value on index (row, col)
    for dir in dirs:
        count += traverse_dir(data, row, col, dir)

    return count

def region_size(row, col, data):
    """Count the number of same values in proximity of index (row, col)
        when traversing both straight and on diagonals.

        :param row: index of initial row for traversal
        :param col: index of initial col for traversal
        :param data: 2D matrix
        :return: size of region
        :raise IndexError: if index (row, col) is not inside the data matrix
    """
    if row >= len(data) or col >= len(data[row]):
        raise IndexError("Index is outside of data range")
    ## get all directions
    dirs = []
    for i in range(-1, 2):
        for j in range(-1, 2):
            if not (i == j == 0):
                dirs.append((i, j))
    ## alternative, list comprehension
    dirs = [(i, j) for i in range(-1, 2) for j in range(-1, 2)
                    if not (i == j == 0)]
    count = 1  ## always min. 1 for value on index (row, col)
    for dir in dirs:
        count += traverse_dir(data, row, col, dir)
    return count

if __name__ == "__main__":
    data = [
        [1, 2, 3],
        [0, 200, 4, 5],
        [0.3021, 4.0]]

    print("---- print_data")
    print_data(data)
    print("---- print_data_aligned")
    print_data_aligned(data)

    ## Matrix from tutorial website
    ## https://cw.fel.cvut.cz/wiki/courses/b4b33rph/cviceni/program_po_tydnech/tyden_05
    mat = [
        [0, 1, 1, 1, 1, 0, 0, 0 ],
        [1, 1, 0, 1, 0, 1, 1, 1 ],
        [0, 1, 1, 0, 0, 1, 0, 1 ],
        [1, 1, 1, 0, 1, 1, 0, 1 ],
        [0, 1, 0, 0, 0, 0, 1, 1 ],
        [1, 0, 1, 1, 0, 0, 0, 0 ],
        [0, 1, 1, 1, 0, 1, 1, 1 ],
        [1, 1, 0, 1, 0, 1, 1, 1 ]]

    ## TODO: try on self-generated random data
    # mat = generate_data(4, 4)

    print("\nRandom matrix: ")
    print_data(mat)
    r, c = 2, 2
    lsize = line_column_size(r, c, mat)
    regsize = region_size(r, c, mat)

    print(f"lsize: {lsize}")
    print(f"{regsize=}")

    ## Example of continue and break; both work also in while loop
    for i in range(15):
        if i == 5:
            continue  ## skip 5, continue for
        elif i == 9:
            break  ## end the cycle execution
        print(i, end=" ")
    print("\nout of loop")

