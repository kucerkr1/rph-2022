class MyMatrix:
    def __init__(self, data=None):
        self.data = data

    def save(self, filename):
        f = open(filename, "w", encoding="utf-8")
        for row in self.data:
            # f.write(",".join(row)) ## same as for below
            for elem in row:
                f.write(str(elem) + ",")
            f.write("\n")
        f.close()

    def load(self, filename):
        with open(filename, "r", encoding="utf-8") as f:
            self.data = []
            for line in f: ## read every line in file
                parts = line.split(",")[:-1]
                elements = [int(e) for e in parts]
                elements = []
                for e in parts:
                    elements.append(int(e))
                self.data.append(elements)

    def get_matrix(self):
        return self.data

if __name__ == "__main__":
    a = MyMatrix([[1, 2, 3], [2, 3, 4]])
    a.save('matrix.txt')
    b = MyMatrix()
    b.load('matrix.txt')
    print(a.get_matrix() == b.get_matrix())