def count_chars_in_string(arg):
    ret_dict = {}
    for char in arg:
        # if char in ret_dict.keys():
        #     ret_dict[char] = ret_dict[char] + 1
        # else:
        #     ret_dict[char] = 1
        ret_dict[char] = ret_dict.get(char, 0) + 1

    return ret_dict

if __name__ == "__main__":
    arg = "aabcdac"
    counted = count_chars_in_string(arg)
    # {'a': 3, 'b': 1, 'c': 2, 'd': 1}
    print(counted)